package ru.polesov.factsaboutcats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import io.realm.Realm
import kotlinx.android.synthetic.main.facts_fragment.*
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


class FactsFragment:Fragment() {

    private val urlCats = "https://api.myjson.com"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.facts_fragment, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getApi().getFactsCats().enqueue(callback)
    }

    private val callback = object : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
            val responseText = response.body()?.string()
              responseText?.let {
                   val factsList = parseResponse(it)
                    saveIntoDB(factsList)
                    showListFromDB()
               }
        }
    }

    override fun onResume() {
        super.onResume()
        showListFromDB()
    }

    private fun getApi(): CatApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(urlCats)
            .build()
        return retrofit.create(CatApi::class.java)
    }

    private fun parseResponse(responseText: String): List<Cat> {
        val factsList: MutableList<Cat> = mutableListOf()
        /*старый api
        val jsonAll = JSONObject(responseText)
        val jsonArray = jsonAll.getJSONArray("all")*/
        val jsonArray = JSONArray(responseText)
        for (index in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(index)
            val catId = jsonObject.getString("id")
            val catText = jsonObject.getString("text")
            val cat = Cat()
            cat.id = catId
            cat.text = catText
            val realm = Realm.getDefaultInstance()
            //предполагаем что id записи уникален, текст может поменяться, признак избранного берем из базы локальной
            val findFact: List<Cat> = realm.where(Cat::class.java).equalTo("id", catId).findAll() as List<Cat>
            if (findFact.isNotEmpty())
                cat.isFavourites = findFact[0].isFavourites
            factsList.add(cat)

        }
        return factsList
    }

    fun saveIntoDB(facts: List<Cat>) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(facts)
        realm.commitTransaction()
    }

    fun showListFromDB() {
        val facts = loadFromDB()
        setList(facts)
    }
    private fun loadFromDB(): List<Cat> {
        val realm = Realm.getDefaultInstance()
        return realm.where(Cat::class.java).findAll()
    }
    private fun setList(facts: List<Cat>) {
        val adapter = FactsAndFavouritesAdapter(facts)
        FactsRecycleView.adapter = adapter
        val layoutManager = LinearLayoutManager(context)
        FactsRecycleView.layoutManager = layoutManager

    }
}