package ru.polesov.factsaboutcats

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ViewPager != null){
            val adapter = TabViewPagerAdapter(supportFragmentManager)
            ViewPager.adapter = adapter
            TabLayout.setupWithViewPager(ViewPager)
        }
    }
}
