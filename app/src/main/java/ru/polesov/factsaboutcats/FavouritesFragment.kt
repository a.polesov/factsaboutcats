package ru.polesov.factsaboutcats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import io.realm.Realm

import kotlinx.android.synthetic.main.favourites_fragment.*

class FavouritesFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.favourites_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        loadFromDB()
    }

    private fun loadFromDB() {
        val realm = Realm.getDefaultInstance()
        val facts : List<Cat> = realm.where(Cat::class.java).equalTo("isFavourites", true).findAll() as List<Cat>
        val adapter = FactsAndFavouritesAdapter(facts)
        FavouritesRecyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(context)
        FavouritesRecyclerView.layoutManager = layoutManager
    }

}
