package ru.polesov.factsaboutcats

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class TabViewPagerAdapter constructor(fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val tabCount = 2
    private val tabName: Array<String> = arrayOf("Факты", "Избранное")
    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position){
            0 -> fragment = FactsFragment()
            1 -> fragment = FavouritesFragment()
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return tabCount
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabName[position]
    }
}