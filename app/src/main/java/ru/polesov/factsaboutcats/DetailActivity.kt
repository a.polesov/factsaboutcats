package ru.polesov.factsaboutcats

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_detail.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class DetailActivity: AppCompatActivity() {

    companion object {
        const val CAT_FACT_TEXT = "cat_fact_text"

        fun openDetailActivity(context: Context, catFactId: String) {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(CAT_FACT_TEXT, catFactId)
            context.startActivity(intent)
        }
    }

    private lateinit var cat:Cat
    private val urlImage = "https://aws.random.cat"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        getApi().getImageCat().enqueue(callback)

        setSupportActionBar(AppToolBar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        intent?.extras?.getString(CAT_FACT_TEXT)?.let {
            val realm = Realm.getDefaultInstance()
            val facts : List<Cat> = realm.where(Cat::class.java).equalTo("id", it).findAll() as List<Cat>
            if (facts.isNotEmpty()){
                cat = facts[0]
                textViewFact.text = cat.text
                labelFavouritesButton()
                buttonEditFavourites.setOnClickListener(buttonOnClick)
            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun labelFavouritesButton(){
        if (cat.isFavourites)
            buttonEditFavourites.setText(R.string.button_delete_favourites)
        else
            buttonEditFavourites.setText(R.string.button_add_favourites)
    }
    
    private val buttonOnClick  = View.OnClickListener {
        val isFav = cat.isFavourites
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        cat.isFavourites = !isFav
        realm.copyToRealmOrUpdate(cat)
        realm.commitTransaction()
        labelFavouritesButton()
    }

    private val callback = object : Callback<ResponseBody> {

        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            t.printStackTrace()
        }

        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
            val responseText = response.body()?.string()
            responseText?.let {
                val image = parseResponse(it)
                Log.d("Detail", "Detail urlImage $image")
                Glide.with(applicationContext)
                    .load(image)
                    .listener(glideListener)
                    .into(imageView)
            }
        }
    }

    private val glideListener = object : RequestListener<Drawable>{
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            progressBar.visibility = View.GONE
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            progressBar.visibility = View.GONE
            return false
        }

    }

    private fun getApi(): ImageApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(urlImage)
            .build()
        return retrofit.create(ImageApi::class.java)
    }

    private fun parseResponse(responseText: String): String{
        val jsonObject = JSONObject(responseText)
        return jsonObject.getString("file")
    }
}