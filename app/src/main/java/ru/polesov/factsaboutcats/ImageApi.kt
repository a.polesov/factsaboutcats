package ru.polesov.factsaboutcats

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface ImageApi {

    @GET("/meow")
    fun getImageCat(): Call<ResponseBody>
}