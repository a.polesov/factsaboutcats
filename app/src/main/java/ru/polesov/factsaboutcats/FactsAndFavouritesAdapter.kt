package ru.polesov.factsaboutcats

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class FactsAndFavouritesAdapter(private val facts: List<Cat>) : RecyclerView.Adapter<FactsAndFavouritesHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactsAndFavouritesHolder {
        val rootView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_facts, parent, false)
        return FactsAndFavouritesHolder(rootView)
    }

    override fun getItemCount(): Int {
        return facts.size
    }

    override fun onBindViewHolder(holder: FactsAndFavouritesHolder, position: Int) {
        holder.bind(facts[position])
    }
}

class FactsAndFavouritesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val textView: TextView = itemView.findViewById(R.id.item_recycler_view_text)

    fun bind(cat: Cat){
        textView.text = cat.text
        itemView.setOnClickListener {
            DetailActivity.openDetailActivity(textView.context, cat.id)
        }
    }
}
