package ru.polesov.factsaboutcats

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface CatApi {

    @GET("/bins/c9uvb")
    fun getFactsCats(): Call<ResponseBody>
}